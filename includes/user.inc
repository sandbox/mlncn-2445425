<?php

/**
 * Sets accounts configuration variables.
 */
function cultura_configure_accounts() {
  // Allow visitor account creation without administrative approval.
  // Account creation restricted by cultura_registration module to token links.
  variable_set('user_register', USER_REGISTER_VISITORS);
  // Allow visitors to complete registration without checking their e-mail.
  variable_set('user_email_verification', FALSE);
  variable_set('anonymous', 'Visitor');
}

/**
 * Sets configuration and creates fields for user profile.
 */
function cultura_configure_user_profile() {
  // Enable user picture support and set the default to a square thumbnail option.
  variable_set('user_pictures', '1');
  variable_set('user_picture_dimensions', '2048x2048');
  variable_set('user_picture_file_size', '1600');
  variable_set('user_picture_style', 'thumbnail');

  // Add full name field to users.
  // TODO check if both field and instance exist for each step.
  $field = array(
    'field_name' => 'full_name',
    'type' => 'text',
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'full_name',
    'entity_type' => 'user',
    'bundle' => 'user',
    'label' => 'Full name',
    'description' => 'Please provide your full name.  (This field is private and seen only by your instructors.)',
    'required' => 1,
    'default_value_function' => '',
    'settings' => array(
      'user_register_form' => 1,
      'better_formats' => array(
        'allowed_formats_toggle' => 0,
        'allowed_formats' => array(
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
        ),
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => '0',
            ),
            'full_html' => array(
              'weight' => '1',
            ),
            'plain_text' => array(
              'weight' => '10',
            ),
          ),
        ),
      ),
    ),
    'widget' => array(
      'weight' => -99,
      'active' => 1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'weight' => 12,
      ),
    ),
    'default_value' => NULL,
  );
  field_create_instance($instance);
}


/**
 * Creates an Administrator user assigned to the "administrator" role.
 */
function cultura_create_administrator_user($administrator_role) {
  $edit = array();
  $edit['name']   = 'Cultura Administrator';
  $edit['mail']   = 'administrator@example.com';
  $edit['roles']  = array($administrator_role->rid => $administrator_role->rid);
  $edit['pass']   = user_password();
  $edit['status'] = 1;
  $account = user_save(drupal_anonymous_user(), $edit);
  drupal_set_message(
    t('Created @role user @name with password %pass — change both the password and e-mail address.',
      array('@role' => $administrator_role->name, '@name' => $edit['name'], '%pass' => $edit['pass']))
  );
  return $account;
}
