<?php

/**
 * Creates content for the welcome article post.
 */
function cultura_content_about() {
  $body = <<<EOD
In this Cultura Exchange you will be connected with students from another country.

Your answers to all questionnaires are kept anonymous.

Your participations in forums are pseudenonymous; if you do not want to be identified choose a pseudonym (username) which is not associated with you elsewhere and keep identifying information out of your comments.
EOD;
  return array(
    'title' => 'Participating in a Cultura Exchange',
    'type' => 'page',
    'promote' => FALSE,
    'sticky' => FALSE,
    'body' => array(
      LANGUAGE_NONE => array(
        '0' => array(
          'value' => $body,
          'summary' => '',
          'format' => 'full_html',
        ),
      ),
    ),
  );
}

/**
 * Creates the About section ("What is Cultura") for a Cultura site.
 */
function cultura_create_about_page($author) {
  $node = cultura_node_save(cultura_content_about(), $author->uid);

  // Create a link to the about page in the main menu.
  $item = array(
    'link_title' => st('About'),
    'link_path' => 'node/' . $node->nid,
    'menu_name' => 'main-menu',
    'weight' => 15,
  );
  menu_link_save($item);

  return $node;
}
