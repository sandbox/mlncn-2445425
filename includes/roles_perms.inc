<?php

/**
 * Sets permissions for system-created anonymous and authenticated user roles.
 */
function cultura_set_permissions_anonymous_authenticated($filtered_html_format) {
  $filtered_html_permission = filter_permission_name($filtered_html_format);
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array(
      ));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array(
      'access content',
      'access comments',
      'search content',
      'use advanced search',
      $filtered_html_permission));
}

/**
 * Creates a role for observers.
 */
function cultura_create_observer_role() {
  $role = new stdClass();
  $role->name = 'observer';
  $role->weight = 20;
  user_role_save($role);
  // Additional permissions beyond what the authenticated user receives.
  $perms = array(
    'access content overview',
    'access toolbar',
    'access contextual links',
    'access dashboard',
    'access site reports',
    'access user profiles',
    'view own unpublished content',
    'view revisions',
    'change own username',
    'view the administration theme',
    'search content',
    'use advanced search',
    'access comments',
    // Statistics
    'access statistics',
    'view post access counter',
  );
  user_role_grant_permissions($role->rid, $perms);
  return $role;
}

/**
 * Creates a role for students.
 */
function cultura_create_student_role($formats) {
  $role = new stdClass();
  $role->name = 'student';
  $role->weight = 30;
  user_role_save($role);
  // Additional permissions beyond what the authenticated user receives.
  $perms = array(
    'change own username',
    'view own unpublished content',
    'search content',
    'access comments',
    'post comments',
    'skip comment approval',
    'use advanced search',
  );
  foreach ($formats as $format) {
    $perms[] = filter_permission_name($format);
  }
  user_role_grant_permissions($role->rid, $perms);
  return $role;
}

/**
 * Creates a role for guest (secondary) instructors (presumed second language).
 */
function cultura_create_guest_instructor_role($formats) {
  $role = new stdClass();
  $role->name = 'guest instructor';
  $role->weight = 40;
  user_role_save($role);
  // Additional permissions beyond what the authenticated user receives.
  $perms = array(
    'access content overview',
    'access toolbar',
    'access contextual links',
    'access dashboard',
    'access user profiles',
    'view own unpublished content',
    'view revisions',
    'revert revisions',
    'change own username',
    'create url aliases',
    'view the administration theme',
    'search content',
    'use advanced search',
    'access comments',
    'administer comments',
    'post comments',
    'skip comment approval',
    'publish button publish own ' . CULTURA_QUESTIONNAIRE_BUILDER_NODE_TYPE,
  );
  foreach ($formats as $format) {
    $perms[] = filter_permission_name($format);
  }
  $content_types = array('page', CULTURA_DISCUSSION_NODE_TYPE, CULTURA_QUESTIONNAIRE_BUILDER_NODE_TYPE);
  foreach ($content_types as $type) {
    $perms[] = "create $type content";
    $perms[] = "delete own $type content";
    $perms[] = "edit any $type content";
    $perms[] = "edit own $type content";
  }
  user_role_grant_permissions($role->rid, $perms);
  return $role;
}

/**
 * Creates a role for host (primary) instrucotrs (de facto administrators).
 */
function cultura_create_host_instructor_role($formats) {
  $role = new stdClass();
  $role->name = 'host instructor';
  $role->weight = 50;
  user_role_save($role);
  // Additional permissions beyond what the authenticated user receives.
  $perms = array(
    'access content overview',
    'access toolbar',
    'customize shortcut links',
    'administer nodes',
    'administer taxonomy',
    'edit terms in 1',
    'edit terms in 2',
    'delete terms in 1',
    'delete terms in 2',
    'administer users',
    'assign roles',
    'access contextual links',
    'access dashboard',
    'access user profiles',
    'access site reports',
    'view own unpublished content',
    'view revisions',
    'revert revisions',
    'change own username',
    'create url aliases',
    'use advanced search',
    'view the administration theme',
    'search content',
    'use advanced search',
    'access comments',
    'administer comments',
    'post comments',
    'skip comment approval',
    'publish button publish any ' . CULTURA_QUESTIONNAIRE_BUILDER_NODE_TYPE,
    // Custom Permissions (config_perms) module defaults
    'administer site information',
    'display site configuration menu',
    // Custom Permissions module created in cultura_add_config_perms().
    'display site configuration system menu',
    'display site configuration people menu',
    'configure custom errors',
    'configure account settings',
    // Statistics
    'access statistics',
    'view post access counter',
  );
  foreach ($formats as $format) {
    $perms[] = filter_permission_name($format);
  }
  $content_types = array('page', CULTURA_DISCUSSION_NODE_TYPE, CULTURA_QUESTIONNAIRE_BUILDER_NODE_TYPE);
  foreach ($content_types as $type) {
    $perms[] = "create $type content";
    $perms[] = "delete any $type content";
    $perms[] = "delete own $type content";
    $perms[] = "edit any $type content";
    $perms[] = "edit own $type content";
  }
  user_role_grant_permissions($role->rid, $perms);
  return $role;
}

/**
 * Creates a role for administrator with all available permissions assigned.
 *
 * I think this role would be better called "developer" or "site builder" but
 * the "administrator" role is the one Drupal core's standard profile gives the
 * "administrator role" setting to which gives all permissions automatically
 * (when a module is installed; it misses later-created permissions).
 */
function cultura_create_developer_role() {
  // Check if 'administrator' role exists first.
  if (!user_role_load_by_name('administrator')) {
    $developer_role = new stdClass();
    $developer_role->name = 'administrator';
    $developer_role->weight = 100;
    user_role_save($developer_role);
    user_role_grant_permissions($developer_role->rid, array_keys(module_invoke_all('permission')));
    // Set this as the administrator role.
    variable_set('user_admin_role', $developer_role->rid);

    // Assign user 1 the developer role.
    db_insert('users_roles')
      ->fields(array('uid' => 1, 'rid' => $developer_role->rid))
      ->execute();
  }
}
