<?php

/**
 * Enable the Bootstrap Cultura theme.
 */
function cultura_configure_themes() {
  // Enables bootsrap_cultura theme and jquery_update module
  theme_enable(array('cultura_bootstrap'));
  theme_disable(array('bartik'));
  variable_set('theme_default', 'cultura_bootstrap');
  variable_set('node_admin_theme', FALSE);

  variable_set('jquery_update_jquery_version', '1.10');
  variable_set('jquery_update_jquery_admin_version', '1.5');
  variable_set('jquery_update_compression_type', 'min');
  variable_set('jquery_update_jquery_cdn', 'none');

  // Enable the admin theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', 'seven')
    ->execute();
  variable_set('admin_theme', 'seven');

  // Enable some standard blocks.
  $default_theme = variable_get('theme_default', 'cultura_bootstrap');
  $admin_theme = 'seven';
  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => -50,
      'region' => 'navigation',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'book',
      'delta' => 'navigation',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => -8,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'node',
      'delta' => 'recent',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'dashboard_main',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'comment',
      'delta' => 'recent',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'new',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'search',
      'delta' => 'form',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => -10,
      'region' => 'dashboard_sidebar',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'cultura_registration',
      'delta' => 'tokens',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => -50,
      'region' => 'dashboard_main',
      'pages' => '',
      'cache' => -1,
    ),
  );

  foreach ($blocks as $block) {
    db_merge('block')
      ->key(array(
        'module' => $block['module'],
        'delta' => $block['delta'],
        'theme' => $block['theme'],
      ))
      ->fields($block)->execute();
  }
}
